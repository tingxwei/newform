import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms'
import { AppComponent } from './app.component';
import { NgModule } from '@angular/core';
import { Component2Component } from './component2/component2.component';


@NgModule({
  declarations: [
    AppComponent,
    Component2Component
  ],
  imports: [
    BrowserModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
